# Dynamically Increment Application version

### Pre-Requisites
Docker

Jenkins

Jenkins Plugin 'Maven'

Git

GitLab

#### Project Outline
In this project we will dive in how to increment version for an applications using a CI pipeline
In versioning there are three parts Major:Minor:Patch
Major Version have huge changes with new features or a bug fix
Minor version has smaller features and bug fixes
Path includes minor bug fixes and minor changes
Depending on the change, we need to increment the version in a pom.xml file
In this Project we will see how can increment different versions for maven


[View Jenkins Jobs Branch for finalised pipeline](https://gitlab.com/FM1995/java-maven-pipeline/-/tree/jenkins-jobs)


#### Getting started

Lets start by utilising the below command in the Git terminal

What this achieves is version manipulation using the Maven Build Helper and Versions plugins. It parses the current project version, increments the next incremental version, and sets the new version for the project. In this case we will increment the patch version and creates a new pom.xml file and replaces it

```
mvn build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion} versions:commit
```

Can see it went from 0.0.1 to 0.0.2

![Image 1](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image1.png)


And can do the same for minor version

```
mvn build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.nextMinorVersion}.${parsedVersion.incrementalVersion} versions:commit
```

Here is the current version

![Image 2](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image2.png)

Testing again using the above command

Can see the minor version bump

![Image 3](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image3.png)

And is now in the pom.xml file

![Image 4](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image4.png)

However executing this in the terminal is not practical, so we will implement this in our CI pipeline to increment our docker image

![Image 5](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image5.png)

We can start by implementing the a new stage in the pipeline, we have to note where to place it. Since we know that the ‘build app’ stage runs ‘mvn packae’ to build a jar file with the version from the pom.xml file, so therefore we would have to define it for the app gets built and that ensures the app gets built with the correct version

And implement it using the shell command

```
sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit
```

![Image 6](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image6.png)

We also have to note that we are working with the docker-image

Lets begin by referencing the pom.xml file

![Image 7](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image7.png)

```
readFile('pom.xml') =~ <version>(.+)</version>
```

Can then put it in a variable

![Image 8](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image8.png)

```
def matcher = readFile('pom.xml') =~ <version>(.+)</version>
```

Can then input the array, where [0] is the version number and [1] is the version tag

![Image 9](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image9.png)

Which references the version in the pom.xml file

![Image 10](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image10.png)

From here it will return <version> and 0.3.0

Can the put it in a variable and append it to build number given in Jenkins

![Image 11](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image11.png)

Which is effectively one of these next to the status

![Image 12](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image12.png)

Can also then put it as an image name for the docker build

![Image 13](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image13.png)

And can be now referenced

![Image 14](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image14.png)


Next we need to modify our Dockerfile so that it takes any version with the java-maven app

![Image 15](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image15.png)

And to ensure we only have one single jar file we can implement the below

![Image 16](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image16.png)


Pipeline now ready to be pushed

![Image 17](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image17.png)

Can then push it to the repo

![Image 18](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image18.png)


New Jenkins file is now available

![Image 19](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image19.png)

Jenkins is now ready to build
Hit build now
Can see it is build 6


![Image 20](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image20.png)

![Image 21](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image21.png)

Can see new field increment version
And can see build is successful
As I did mvn clean package it re-packed it and done the versioning using mvn clean package

![Image 22](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image22.png)

And was an incremental version as defined in the code

Can also see the new docker image version

![Image 23](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image23.png)


Can also see the build number being put into the docker image version

![Image 24](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image24.png)

Can now see in my docker the hub the new images tagged with the version number

![Image 25](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image25.png)

Issue is that it keeps on reverting to the old version which is 1.1.0 instead of 1.1.1

![Image 26](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image26.png)

Reason being it doesn’t get updated in the git repo just locally on the Jenkins server
Now when another team like developers, they can pull the changes with the latest version and continue the versioning from the latest
Next step we will implement another stage on the pipeline called ‘commit version update’

![Image 27](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image27.png)

![Image 28](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image28.png)

Ready for build

Can see commit version update

![Image 29](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image29.png)

Can see the below changes

![Image 30](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image30.png)

Pom.xml was changed with a new version
Going into the repo, can the see the version upgrade aswell as the ‘jenkins’ user

![Image 31](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image31.png)

Below is the change

![Image 32](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image32.png)

A new Docker image was also generated

![Image 33](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image33.png)

With a reference of the build

![Image 34](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image34.png)

However this will cause a loop of continuous builds and ignore the
As we cab see below, lets make a code change and commit it
New code block


![Image 35](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image35.png)

Code ready to be pushed

![Image 36](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image36.png)

Pipeline getting pushed automatically

![Image 37](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image37.png)

And another gets completed

![Image 38](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image38.png)

On build 12 it gets bumped up a version

![Image 39](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image39.png)

As I triggered the commit loop it went and pushed it up until build 15

![Image 40](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image40.png)

![Image 41](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image41.png)

Now we need a solution for the loop
We need protocol in place to send us a notification that it is 
Can install a plugin called committer strategy

![Image 42](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image42.png)

![Image 43](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image43.png)

New configuration includes the below

![Image 44](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image44.png)

As previously done the emails configured was below

![Image 45](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image45.png)

We can configure it for the below, where every commit by this user email it can be ignored and allow builds for any other authors

![Image 46](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image46.png)

Reverting the previous code and testing a new change

Adding ‘target’ in the gitignore

![Image 47](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image47.png)

![Image 48](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image48.png)

New build

![Image 49](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image49.png)

No more endless commits as there due to the ignore of the email and plugin

![Image 50](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image50.png)

Can see in the below the commit has completed with a new version

![Image 51](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image51.png)

And new docker repo with the build number has also been deployed

![Image 52](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image52.png)

![Image 53](https://gitlab.com/FM1995/dynamically-increment-application-version-2024/-/raw/main/Images/Image53.png)

